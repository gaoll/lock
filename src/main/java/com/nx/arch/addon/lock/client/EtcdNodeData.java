package com.nx.arch.addon.lock.client;

import java.util.List;

import com.alibaba.fastjson.JSON;

/**
 * @类名称 EtcdNodeData.java
 * @类描述 节点数据对象
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年3月28日             
 *     ----------------------------------------------
 * </pre>
 */
public class EtcdNodeData {
    public String key;
    
    public long createdIndex;
    
    public long modifiedIndex;
    
    public String value;
    
    public String expiration;
    
    public Integer ttl;
    
    public boolean dir;
    
    public List<EtcdNodeData> nodes;
    
    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
